Cause:
Exp1:
Data -> 1856
Time -> 1 hours/epoch
Loss -> started with 3.2 and came to 2.5

Exp2
Data -> 16
Time -> 30Secs/Epoch
Loss -> Started 3.07 -> Stayed 3.07

Exp3 -> Incresaing Lr from Exp2 to 0.1
Data -> 16
Loss is oscillating between epochs

Exp 4:
Lr = 0.01
Embedding - GPT2
hidden_layer = 16
started - 2.93
2.79
2.71
2.67
2.40 - 16 epochs

Exp 5:
Lr = 0.01
Embedding - USE
Data - 16
hidden_layer = 16
2.39 - 20 epochs

Exp 6:
Lr = 0.01
Embedding - USE
Data - full
hidden_layer = 16
start with 2.9
2.47 - after 17 epochs

Emotion:
Exp 1:
Lr = 0.01
Embedding - BERT
Data - full
hidden_layer = 16
start with 2.51
2.31 - after 18 epochs
Precision: 0.6181986711553167  Recall: 0.647353780579587 F1:  0.6219283197007781 Accuracy:  0.7602179836512262
Precision at 3 macro metrics: 0.9366941640132089  Recall at k: 0.9388528917330761 F1 at k:  0.929157614414813 Accuracy:  0.9536784741144414
