In this paper we are trying to predict emotion-cause pair for a given document.


**FOLDER INFORMATION**

Data -> Holds the raw data which is in chinese

DataPreprocessing -> This module is responsible for converting data to english, cleaning and preprocessing it.

Evaluation -> Evaluation Metrics for Emotion/Cause

Models -> BilSTM model for extraction emotion and causes

Pair_Filtering -> Model to suggest correct emotion cause pair

Sentence_Embedding -> Holds embedding information. Currently 3 types of embedding is possible. BERT, USE and GPT2

Test -> Various unit test to verify functionality of model

Trainer -> Holds Model training for emotion and cause

Utility -> Information related to configurations.


run.py -> runs the model.

**REFERENCES**
1. https://www.aclweb.org/anthology/P19-1096.pdf
