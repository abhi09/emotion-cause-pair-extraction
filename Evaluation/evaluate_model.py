from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score


# This function evaluates model over validation/test data.
def metrics(trainer, val_data):
    y_true = []
    y_pred = []

    for data in val_data:
        sentence, target = data
        try:
            scores = trainer.predict(sentence).detach().cpu().clone().numpy()
            target = target - 1
            y_pred.append(scores.argmax().tolist())
            y_true.append(target.tolist()[0])
        except:
            print("Thrown error for predicting sentence: ", sentence)

    precision = precision_score(y_true, y_pred, average='micro')
    recall = recall_score(y_true, y_pred, average='micro')
    f1 = f1_score(y_true, y_pred, average='micro')
    acc = accuracy_score(y_true, y_pred)
    return precision, recall, f1, acc


# This function evaluates model over validation/test data for top k values.
def metrics_at_k(trainer, val_data, k):
    y_true = []
    y_pred = []

    for data in val_data:
        sentence, target = data
        try:
            target = target - 1
            scores = trainer.predict(sentence).detach().cpu().clone()
            pred_k = scores.numpy().squeeze().argsort()[::-1][:k]
            true = target.tolist()[0]
            append = False
            for pred in pred_k:
                if pred == target:
                    y_pred.append(pred)
                    y_true.append(target.tolist()[0])
                    append = True
            if append is False:
                y_pred.append(pred_k[0])
                y_true.append(target.tolist()[0])
        except Exception as e:
            print(e)
            print("Thrown error for predicting sentence: ", sentence)

    if len(y_true) != len(y_pred):
        return None, None, None
    precision = precision_score(y_true, y_pred, average='micro')
    recall = recall_score(y_true, y_pred, average='micro')
    f1 = f1_score(y_true, y_pred, average='micro')
    acc = accuracy_score(y_true, y_pred)
    return precision, recall, f1, acc
