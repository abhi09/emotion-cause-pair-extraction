from unittest import TestCase

from Trainer.EmotionCauseDataLoader import EmotionCauseDataLoader


class TestEmotionCauseDataLoader(TestCase):
    def test_get_data_loader(self):
        dt_loader = EmotionCauseDataLoader()
        for a in dt_loader.get_data_loader():
            self.assertIsNotNone(a)
