from unittest import TestCase

from Trainer.PrepareDataForInput import PrepareDataForInput


class TestPrepareDataForInput(TestCase):
    def test__prepare_data_for_cause(self):
        obj = PrepareDataForInput()
        data = obj.prepare_data_for_cause()
        self.assertEqual(2, len(data))
