import unittest

from Models import BiLSTM as blstm

class BiLSTM(unittest.TestCase):
    def test_initialize_model(self):
        bi_lstm_model = blstm.BiLSTM('GPT2',16,12)
        self.assertIsNotNone(bi_lstm_model)

    def test_forward(self):
        bi_lstm_model = blstm.BiLSTM('GPT2', 16, 2)
        bi_lstm_model.forward(["This is a sentence","This is next sentence"])


if __name__ == '__main__':
    unittest.main()
