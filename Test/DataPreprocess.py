import unittest

from DataPreprocessing import data_process as dp

class DataPreprocess(unittest.TestCase):
    def test_load_data(self):
        df = dp.data_proc()[0]
        self.assertEqual((18,5),df.shape)

if __name__ == '__main__':
    unittest.main()
