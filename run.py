from Trainer import Train, EmotionCauseDataLoader, LayerTwoData
from Utility import configuration as conf
from Pair_Filtering import LR_Train_CrossEncoder
from numpy import save, load
import os.path


def main(layer=1):
    if layer == 1:
        if not conf.emotion_data:
            print("Running For Cause")
            data_loader = EmotionCauseDataLoader.EmotionCauseDataLoader()
            data_loader_train = data_loader.get_data_loader()
            data_loader_val = data_loader.get_data_loader_val()
            data_loader_test = data_loader.get_data_loader_test()

            trainer = Train.Train(data_loader_train, data_loader_val)
            trainer.initialize_model('BiLSTM', 'BERT', conf.hidden_layer, 1)
            trainer.initialize_loss_function('CrossEntropyLoss')
            trainer.initialize_optimizer('Adam', conf.learning_rate, conf.momentum)
            trainer.train_epochs(conf.num_epochs)
        else:
            print("Running For Emotion")
            data_loader = EmotionCauseDataLoader.EmotionCauseDataLoader()
            data_loader_train = data_loader.get_data_loader()
            data_loader_val = data_loader.get_data_loader_val()
            data_loader_test = data_loader.get_data_loader_test()

            trainer = Train.Train(data_loader_train, data_loader_val)
            trainer.initialize_model('BiLSTM', 'BERT', conf.hidden_layer, 1)
            trainer.initialize_loss_function('CrossEntropyLoss')
            trainer.initialize_optimizer('Adam', conf.learning_rate, conf.momentum)
            trainer.train_epochs(conf.num_epochs)
    else:
        l2data = LayerTwoData.LayerTwoData()
        # train data
        if os.path.isfile(conf.lr_train_data):
            print("Loading training data for filtering from file")
            train_data = load(conf.lr_train_data, allow_pickle=True)
        else:
            train_data = l2data.give_sentence(0)
            save(conf.lr_train_data, train_data)

        # val data
        if os.path.isfile(conf.lr_val_data):
            print("Loading validation data for filtering from file")
            val_data = load(conf.lr_val_data, allow_pickle=True)
        else:
            val_data = l2data.give_sentence(1)
            save(conf.lr_val_data, val_data)

        # test data
        '''if os.path.isfile(conf.lr_test_data):
            print("Loading test data for filtering from file")
            test_data = load(conf.lr_test_data, allow_pickle = True)
        else:
            test_data = l2data.give_sentence(2)
            save(conf.lr_test_data, test_data)'''

        # val_data = l2data.give_sentence(1)

        lr_trainer = LR_Train_CrossEncoder.Train(train_data, val_data)
        lr_trainer.initialize_model()
        lr_trainer.train_epochs()


if __name__ == "__main__":

    conf.emotion_data = False
    conf.ckpt_save_path = conf.ckpt_save_path_cause

    if not os.path.isfile(conf.ckpt_save_path):
        main(1)

    conf.emotion_data = True
    conf.ckpt_save_path = conf.ckpt_save_path_emotion
    if not os.path.isfile(conf.ckpt_save_path):
        main(1)

    if not os.path.isfile(conf.lr_chkpt):
        main(2)
