from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from Utility import configuration as conf


# Evaluation metrics for pair filtering.
def metrics(trainer, val_data):
    y_true = []
    y_pred = []

    for emotion, cause, distance, target in val_data:
        try:
            score = trainer.predict([[emotion, cause]])
            if score[0] > conf.LR_threshold:
                y_pred.append(1)
            else:
                y_pred.append(0)
            y_true.append(int(target))
        except:
            print("Thrown error for predicting sentence in LR: ")

    precision = precision_score(y_true, y_pred, average='micro')
    recall = recall_score(y_true, y_pred, average='micro')
    f1 = f1_score(y_true, y_pred, average='micro')
    acc = accuracy_score(y_true, y_pred)
    print("confusion matrix", confusion_matrix(y_true, y_pred))
    return precision, recall, f1, acc