from torch.utils.data import DataLoader

import Pair_Filtering.LR_Evaluation as eval
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
import torch
from Utility import configuration
import tqdm as tqdm
import os.path
from sentence_transformers import InputExample
from sentence_transformers.cross_encoder import CrossEncoder


class Train:
    __model = None
    __loss = None
    __optimizer = None

    def __init__(self, input_data, validation_data):
        self.input_data = input_data
        self.validation_data = validation_data
        self.writer = SummaryWriter()
        self.__loss = nn.BCELoss()

    def initialize_model(self):
        self.__model = CrossEncoder('sentence-transformers/ce-distilroberta-base-stsb', num_labels=1
                                    , device=configuration.device)
        if os.path.isfile(configuration.lr_chkpt):
            print("Loading From CheckPoint Model")
            self.__model = CrossEncoder(configuration.lr_chkpt, device=configuration.device)

    def train_epochs(self):
        train_samples = []
        for emotion, cause, distance, target in tqdm.tqdm(self.input_data):
            try:
                if not ('<EOF>' in emotion or '<EOF>' in cause):
                    train_samples.append(
                        InputExample(texts=[emotion, cause], label=int(target))
                    )
            except Exception as e:
                print("Thrown Error for ", e)

        print('Finished Processing!')
        # model = CrossEncoder('sentence-transformers/ce-distilroberta-base-stsb', num_labels=1)
        train_dataloader = DataLoader(train_samples, shuffle=True, batch_size=16)
        self.__model.fit(train_dataloader=train_dataloader, epochs=20, warmup_steps=10)
        print('Finished Training!')
        self.__model.save_pretrained(configuration.lr_chkpt)
        precision, recall, f1, acc = eval.metrics(self.__model, self.validation_data)
        print("Precision at k:", precision, " Recall at k:", recall, "F1 at k: ", f1, "Accuracy: ", acc)
        self.writer.close()

    def predict(self, emotion, cause, distance):
        with torch.no_grad():
            return self.__model.predict(emotion, cause, distance)