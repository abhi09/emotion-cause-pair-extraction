import Pair_Filtering.LR_Evaluation as eval
import Pair_Filtering.logistic_regression as logreg
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import torch
from Utility import configuration
import tqdm as tqdm
import os.path


# Train Model for pair filtering.
class Train:
    __model = None
    __loss = None
    __optimizer = None

    def __init__(self, input_data, validation_data):
        self.input_data = input_data
        self.validation_data = validation_data
        self.writer = SummaryWriter()
        self.__loss = nn.BCELoss()

    def initialize_model(self, embedder_name):
        self.__model = logreg.LogisticRegressionCustom(embedder_name)
        if os.path.isfile(configuration.lr_chkpt):
            print("Loading From CheckPoint Model")
            self.__model, _, _ = self.load_ckp_model(configuration.lr_chkpt, self.__model)
        if torch.cuda.device_count() > 1:
            print("Using ", torch.cuda.device_count(), "GPUs for Logistic regression")
            self.__model = nn.DataParallel(self.__model)
        self.__model.to(configuration.device)
        self.__optimizer = optim.Adam(self.__model.parameters(),
                                      lr = configuration.Logistic_Regression_Learning_Rate,
                                      betas = (0.9, 0.999), eps = 1e-08, weight_decay = 0,
                                      amsgrad = True)
        if os.path.isfile(configuration.lr_chkpt):
            print("Loading From CheckPoint Optimizer")
            self.__optimizer, _, _ = self.load_ckp_optimizer(configuration.lr_chkpt,self.__optimizer)


    def train_epochs(self, num_epochs):
        total_loss_list = []
        for epoch in range(num_epochs):
            total_loss = 0
            total_samples = 0
            print("Running Epoch : {}".format(epoch))
            loss = 0
            for emotion, cause, distance, target in tqdm.tqdm(self.input_data):
                try:
                    total_samples += 1
                    self.__model.zero_grad()
                    score = self.__model(emotion, cause, distance)
                    target = torch.tensor([target]).unsqueeze(0).type(torch.float)
                    loss = self.__loss(score.unsqueeze(0),target.to(configuration.device))
                    total_loss += loss.item()
                    self.writer.add_scalar("Loss/train", loss.item(), epoch)
                    loss.backward()
                    torch.nn.utils.clip_grad_norm_(self.__model.parameters(), configuration.Logistic_Regression_Clipping_value)
                    self.__optimizer.step()
                except Exception as e:
                    print("Thrown Error for ", emotion)

            print('Finished Training!')

            checkpoint = {
                'epoch': epoch + 1,
                # 'valid_loss_min': valid_loss,
                'state_dict': self.__model.state_dict(),
                'optimizer': self.__optimizer.state_dict(),
                'loss': loss,
            }

            total_loss_list.append(total_loss / total_samples)
            print('Average Loss : ', total_loss_list[-1])


            precision, recall, f1, acc = eval.metrics(self, self.validation_data)
            print("Precision:", precision, " Recall:", recall, "F1: ", f1, "Accuracy: ", acc)

            self.save_ckp(checkpoint, configuration.lr_chkpt)
        self.writer.close()


    def predict(self, emotion, cause, distance):
        with torch.no_grad():
            return self.__model(emotion, cause, distance)

    def save_ckp(self, state, checkpoint_path):
        """
        state: checkpoint we want to save
        is_best: is this the best checkpoint; min validation loss
        checkpoint_path: path to save checkpoint

        """
        f_path = checkpoint_path
        # save checkpoint data to the path given, checkpoint_path
        torch.save(state, f_path)

    def load_ckp_model(self,checkpoint_fpath, model):
        """
        checkpoint_path: path to save checkpoint
        model: model that we want to load checkpoint parameters into
        optimizer: optimizer we defined in previous training
        """
        # load check point
        checkpoint = torch.load(checkpoint_fpath, map_location=configuration.device)
        # initialize state_dict from checkpoint to model
        model.load_state_dict(checkpoint['state_dict'])
        # initialize valid_loss_min from checkpoint to valid_loss_min
        return model, checkpoint['epoch'], checkpoint['loss']

    def load_ckp_optimizer(self,checkpoint_fpath, optimizer):
        """
        checkpoint_path: path to save checkpoint
        model: model that we want to load checkpoint parameters into
        optimizer: optimizer we defined in previous training
        """
        # load check point
        checkpoint = torch.load(checkpoint_fpath, map_location=configuration.device)
        # initialize optimizer from checkpoint to optimizer
        optimizer.load_state_dict(checkpoint['optimizer'])
        # initialize valid_loss_min from checkpoint to valid_loss_min
        # valid_loss_min = checkpoint['valid_loss_min']
        # return model, optimizer, epoch value, min validation loss
        return optimizer, checkpoint['epoch'], checkpoint['loss']


