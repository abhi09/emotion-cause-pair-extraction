import torch
import torch.nn as nn
import torch.nn.functional as F
from Sentence_Embedding import embedding_generation as embed
from Utility.configuration import device


# LR Model for Pair Filtering.

class LogisticRegressionCustom(nn.Module):

    # __inner_layer_dim = 64

    def __init__(self, embedder_name):
        super(LogisticRegressionCustom, self).__init__()
        self.embedding = embed.Embedding(embedder_name)
        embed_len = self.embedding.get_embedding_length()
        self.fc1 = nn.Linear(embed_len * 2 + 1, int(embed_len / 2))
        # self.fc2 = nn.Linear(embed_len, int(embed_len / 2))
        self.fc3 = nn.Linear(int(embed_len/ 2), 1)
        # self.fc4 = nn.Linear(64, 1)
        self.dropout = nn.Dropout(0.25)

    def forward(self, emotion, cause, distance):
        em = self.embedding.get_embedding(emotion)
        ca = self.embedding.get_embedding(cause)
        dist = torch.tensor([distance]).unsqueeze(0).view(-1)
        data = torch.cat((em.view(-1), ca.view(-1), dist), 0).to(device = device)

        hidden_1 = F.leaky_relu(self.fc1(data))
        # hidden_1 = self.dropout(hidden_1)
        hidden_2 = F.leaky_relu(self.fc3(hidden_1))
        # hidden_2 = self.dropout(hidden_2)
        # hidden_3 = F.leaky_relu(self.fc3(hidden_2))
        # hidden_4 = F.leaky_relu(self.fc4(hidden_3))
        output = torch.sigmoid(hidden_2)
        return output