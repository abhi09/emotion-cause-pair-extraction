import torch
MODE = "DEV" #Prod
#MODE = "Prod"

from pathlib import Path

def get_project_root():
    return Path(__file__).parent.parent

ROOT_DIR = get_project_root()

file_import_path = '/sample_data_pair_eng.txt' if MODE == "DEV" else '/all_data_pair_eng.txt'
file_import_path = str(ROOT_DIR) + '/DataPreprocessing' + file_import_path


emotion_data = True
num_epochs = 2
hidden_layer = 16
sentence_padding = 25
learning_rate = 0.001
momentum = 0.9

clip = 1.00

metrics_k = 3

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
ckpt_save_path_emotion = str(ROOT_DIR) + '/CheckPoint/current_checkpoint_emotion.pt'
ckpt_save_path_cause = str(ROOT_DIR) + '/CheckPoint/current_checkpoint.pt'

if emotion_data:
    ckpt_save_path = ckpt_save_path_emotion
else:
    ckpt_save_path = ckpt_save_path_cause

Logistic_Regression_Embedder = 'BERT'
Logistic_Regression_Learning_Rate = 0.01
Logistic_Regression_Num_Epochs = 10
Logistic_Regression_Clipping_value = 1
LR_threshold = 0.5
lr_train_data = str(ROOT_DIR) + '/Pair_Filtering/layer2traindata.npy'
lr_test_data = str(ROOT_DIR) + '/Pair_Filtering/layer2testdata.npy'
lr_val_data = str(ROOT_DIR) + '/Pair_Filtering/layer2valdata.npy'
lr_chkpt = str(ROOT_DIR) + '/CheckPoint/current_checkpoint_LR.pt'

if emotion_data:
    best_model = str(ROOT_DIR) + '/Model/best_model_emotion.pt'
else:
    best_model = str(ROOT_DIR) + '/Model/best_model_cause.pt'

