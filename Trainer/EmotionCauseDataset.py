from torch.utils.data import Dataset
from Trainer import PrepareDataForInput
from Utility.configuration import emotion_data


# Data Set object.
class EmotionCauseDataset(Dataset):

    __cause_data = None

    def __init__(self,isLevelTwo = False):
        if not isLevelTwo:
            data_model = PrepareDataForInput.PrepareDataForInput()
            if not emotion_data:
                self.__cause_data = data_model.prepare_data_for_cause()
            else:
                self.__cause_data = data_model.prepare_data_for_emotion()
        else:
            data_model = PrepareDataForInput.PrepareDataForInput()
            self.__cause_data = data_model.prepare_data_for_emotion_cause()

    def __len__(self):
        return len(self.__cause_data)

    def __getitem__(self, item):
        return  self.__cause_data[item][0],self.__cause_data[item][1]