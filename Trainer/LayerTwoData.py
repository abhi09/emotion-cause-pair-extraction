from Trainer.EmotionCauseDataLoader import EmotionCauseDataLoader
from Trainer.Train import Train
from Utility import configuration as conf
import numpy as np


# Layer Two data object.
class LayerTwoData:
    def __init__(self):
        data_loader = EmotionCauseDataLoader(True)
        self.data_loader_train = data_loader.get_data_loader()
        self.data_loader_val = data_loader.get_data_loader_val()
        self.data_loader_test = data_loader.get_data_loader_test()

        conf.emotion_data = False
        self.cause_model = Train(None, None)
        self.cause_model.initialize_model('BiLSTM', 'BERT', conf.hidden_layer, 1)
        self.cause_model.initialize_loss_function('CrossEntropyLoss')
        self.cause_model.initialize_optimizer('Adam', conf.learning_rate, conf.momentum)

        conf.emotion_data = True
        self.emotion_model = Train(None, None)
        self.emotion_model.initialize_model('BiLSTM', 'BERT', conf.hidden_layer, 1)
        self.emotion_model.initialize_loss_function('CrossEntropyLoss')
        self.emotion_model.initialize_optimizer('Adam', conf.learning_rate, conf.momentum)

    def give_sentence(self, data_type=0):
        final_data = []
        data_to_process = self.data_loader_train
        if data_type == 1:
            data_to_process = self.data_loader_val
        if data_type == 2:
            data_to_process = self.data_loader_test

        for sentence, target in data_to_process:
            try:
                cause_index = self.cause_model.predict(sentence).detach().cpu().clone()
                emotion_index = self.emotion_model.predict(sentence).detach().cpu().clone()

                pred_k_cause_index = cause_index.numpy().squeeze().argsort()[::-1][:conf.metrics_k]
                pred_k_emotion_index = emotion_index.numpy().squeeze().argsort()[::-1][:conf.metrics_k]

                correct_index_emotion, correct_index_cause = target[0] - 1, target[1] - 1
                for i in range(conf.metrics_k):
                    for j in range(conf.metrics_k):
                        t_val = 0
                        if pred_k_cause_index[i] == correct_index_cause and\
                                pred_k_emotion_index[j] == correct_index_emotion:
                            t_val = 1

                        final_data.append((sentence[pred_k_emotion_index[j]][0][0],
                                           sentence[pred_k_cause_index[i]][0][0],
                                           abs(pred_k_cause_index[i] - pred_k_emotion_index[j]), t_val))
            except Exception as e:
                print("Exception Thrown")
        return np.asarray(final_data)
