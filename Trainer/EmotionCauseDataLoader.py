from torch.utils.data import DataLoader

from Trainer import EmotionCauseDataset
import torch


# Data Loader object.
class EmotionCauseDataLoader:

    __data_loader_train = None
    __data_loader_val = None
    __data_loader_test = None

    def __init__(self, isLevelTwo = False):
        if not isLevelTwo:
            emcds = EmotionCauseDataset.EmotionCauseDataset()
            total_size = len(emcds)
            train_size = total_size * 3 // 5
            val_size = total_size // 5
            test_size = total_size - (train_size + val_size)
            train_ds, val_ds, test_ds = torch.utils.data.random_split(emcds, [train_size, val_size, test_size])
            self.__data_loader_train = DataLoader(train_ds,batch_size=1,shuffle=True,num_workers=0)
            self.__data_loader_val = DataLoader(val_ds, batch_size=1, shuffle=True, num_workers=0)
            self.__data_loader_test = DataLoader(test_ds, batch_size=1, shuffle=True, num_workers=0)
        else:
            emcds = EmotionCauseDataset.EmotionCauseDataset(True)
            total_size = len(emcds)
            train_size = total_size * 3 // 5
            val_size = total_size // 5
            test_size = total_size - (train_size + val_size)
            train_ds, val_ds, test_ds = torch.utils.data.random_split(emcds, [train_size, val_size, test_size])
            self.__data_loader_train = DataLoader(train_ds, batch_size=1, shuffle=True, num_workers=0)
            self.__data_loader_val = DataLoader(val_ds, batch_size=1, shuffle=True, num_workers=0)
            self.__data_loader_test = DataLoader(test_ds, batch_size=1, shuffle=True, num_workers=0)

    def get_data_loader(self):
        return self.__data_loader_train

    def get_data_loader_val(self):
        return self.__data_loader_val

    def get_data_loader_test(self):
        return self.__data_loader_test