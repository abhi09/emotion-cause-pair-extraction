import Evaluation.evaluate_model as eval
import Models.BiLSTM as BiLSTM
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import torch
from Utility import configuration
import tqdm as tqdm
import os.path

from Utility.configuration import device


# Train Object for Emotion/Cause
class Train:
    __model = None
    __loss = None
    __optimizer = None

    def __init__(self, input_data, validation_data):
        self.input_data = input_data
        self.validation_data = validation_data
        self.writer = SummaryWriter()

    def initialize_model(self, model_name, embedder_name, hidden_dim, target_size):
        switcher = {
            'BiLSTM':
                self.__bert_model_init
        }
        self.__model = switcher.get(model_name, None)(embedder_name, hidden_dim, target_size)
        if os.path.isfile(configuration.ckpt_save_path):
            print("Loading From CheckPoint Model")
            self.__model, _, _ = self.load_ckp_model(configuration.ckpt_save_path, self.__model)
        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            self.__model = nn.DataParallel(self.__model)
        self.__model.to(configuration.device)

    def __bert_model_init(self, embedder_name, hidden_dim, target_size):
        return BiLSTM.BiLSTM(embedder_name, hidden_dim, target_size)

    def initialize_loss_function(self, loss_func_name):

        switcher = {
            'NLLLoss':
                self.__nll_loss,
            'CrossEntropyLoss':
                self.__cross_entropy_loss
        }

        self.__loss = switcher.get(loss_func_name, None)()

    def __nll_loss(self):
        return nn.NLLLoss()

    def __cross_entropy_loss(self):
        return nn.CrossEntropyLoss()

    def initialize_optimizer(self, optim_name, learning_rate, momentum):
        switcher = {
            'SGD':
                self.__init_sgd,
            'Adam': self.__init_adam
        }

        self.__optimizer = switcher.get(optim_name)(learning_rate, momentum)
        if os.path.isfile(configuration.ckpt_save_path):
            print("Loading From CheckPoint Optimizer")
            self.__optimizer, _, _ = self.load_ckp_optimizer(configuration.ckpt_save_path, self.__optimizer)

    def __init_sgd(self, learning_rate, momentum):
        return optim.SGD(self.__model.parameters(), lr=learning_rate, momentum=momentum)

    def __init_adam(self, learning_rate, momentum=1.0):
        return optim.Adam(self.__model.parameters(), lr=learning_rate, betas=(0.9, 0.999), eps=1e-08,
                          weight_decay=0,
                          amsgrad=True)

    def train_epochs(self, num_epochs):
        if os.path.isfile(configuration.best_model):
            print("Loading best saved model from file")
            _, best_metric = self.load_model(configuration.best_model)
        else:
            best_metric = 0

        total_loss_list = []
        for epoch in range(num_epochs):
            total_loss = 0
            total_samples = 0
            print("Running Epoch : {}".format(epoch))
            loss = 0
            for sentence, target in tqdm.tqdm(self.input_data):
                try:
                    total_samples += 1
                    self.__model.zero_grad()
                    scores = self.__model(sentence)
                    target = torch.tensor([target - 1])
                    loss = self.__loss(scores.view(1, -1), target.to(configuration.device))
                    total_loss += loss.item()
                    loss.backward()
                    torch.nn.utils.clip_grad_norm_(self.__model.parameters(), configuration.clip)
                    self.__optimizer.step()
                except:
                    print("Thrown Error for ", sentence)

            print('Finished Training!')

            checkpoint = {
                'epoch': epoch + 1,
                'state_dict': self.__model.state_dict(),
                'optimizer': self.__optimizer.state_dict(),
                'loss': loss,
            }

            total_loss_list.append(total_loss / total_samples)
            print('Average Loss : ', total_loss_list[-1])
            self.writer.add_scalar("Loss/train", total_loss_list[-1], epoch)

            precision, recall, f1, acc = eval.metrics(self, self.validation_data)
            print("Precision:", precision, " Recall:", recall, "F1: ", f1, "Accuracy: ", acc)

            if f1 > best_metric:
                best_metric = f1
                self.save_model(self.__model, best_metric, configuration.best_model)

            precision, recall, f1, acc = eval.metrics_at_k(self, self.validation_data, k=configuration.metrics_k)
            print("Precision at k:", precision, " Recall at k:", recall, "F1 at k: ", f1, "Accuracy: ", acc)

            self.save_ckp(checkpoint, configuration.ckpt_save_path)
        self.writer.close()

    def save_model(self, model, metric, path):
        torch.save({'model': model, 'metric': metric}, path)

    def load_model(self, path):
        model_values = torch.load(path, map_location=device)
        model = model_values['model']
        metric = model_values['metric']
        return model, metric
        
    def save_ckp(self, state, checkpoint_path):
        """
        state: checkpoint we want to save
        is_best: is this the best checkpoint; min validation loss
        checkpoint_path: path to save checkpoint

        """
        f_path = checkpoint_path
        # save checkpoint data to the path given, checkpoint_path
        torch.save(state, f_path)

    def load_ckp_model(self, checkpoint_fpath, model):
        """
        checkpoint_path: path to save checkpoint
        model: model that we want to load checkpoint parameters into
        optimizer: optimizer we defined in previous training
        """
        # load check point
        '''if torch.cuda.is_available():
            checkpoint = torch.load(checkpoint_fpath)
        else:
            checkpoint = torch.load(checkpoint_fpath, map_location=torch.device('cpu'))'''
        checkpoint = torch.load(checkpoint_fpath, map_location=device)
        # initialize state_dict from checkpoint to model
        model.load_state_dict(checkpoint['state_dict'])
        # initialize valid_loss_min from checkpoint to valid_loss_min
        # valid_loss_min = checkpoint['valid_loss_min']
        # return model, optimizer, epoch value, min validation loss
        return model, checkpoint['epoch'], checkpoint['loss']

    def load_ckp_optimizer(self, checkpoint_fpath, optimizer):
        """
        checkpoint_path: path to save checkpoint
        model: model that we want to load checkpoint parameters into
        optimizer: optimizer we defined in previous training
        """
        # load check point
        checkpoint = torch.load(checkpoint_fpath, map_location=device)

        # initialize optimizer from checkpoint to optimizer
        optimizer.load_state_dict(checkpoint['optimizer'])

        # initialize valid_loss_min from checkpoint to valid_loss_min
        return optimizer, checkpoint['epoch'], checkpoint['loss']

    # Save and Load Functions
    def predict(self, input_data):
        with torch.no_grad():
            return self.__model(input_data)
