from DataPreprocessing import data_process as dp
from Utility import configuration as conf
import numpy as np


# Prepares Data.
class PrepareDataForInput:
    __df = None

    def __init__(self):
        self.__df = dp.data_proc()

    # This method should return a List of List sentences and index of which sentence is cause also it should do the
    # padding
    def prepare_data_for_cause(self) -> np.ndarray:
        cause_data_frame = self.__df[0][['Document no', 'cause', 'Clauses']]
        cause_data_frame['Clauses'].replace('\n', '', regex=True, inplace=True)
        cause_data_frame['Clauses'] = cause_data_frame['Clauses'].str.lower()
        final_list = []

        for index in cause_data_frame['Document no'].unique():
            my_clause_list = []
            my_clause_list_df = cause_data_frame[cause_data_frame['Document no'] == index]['Clauses']
            for ind in my_clause_list_df.index:
                my_clause_list.append([my_clause_list_df[ind]])
            while len(my_clause_list) < conf.sentence_padding:
                my_clause_list.append(["<EOF>"])
            my_clause_list = my_clause_list[:conf.sentence_padding]
            cause = int(cause_data_frame[cause_data_frame['Document no'] == index]['cause'].values.tolist()[0])
            final_list.append([my_clause_list, cause])

        return np.asarray(final_list)

    def prepare_data_for_emotion(self) -> np.ndarray:
        cause_data_frame = self.__df[0][['Document no', 'emotion', 'Clauses']]
        cause_data_frame['Clauses'].replace('\n', '', regex=True, inplace=True)
        cause_data_frame['Clauses'] = cause_data_frame['Clauses'].str.lower()
        final_list = []

        for index in cause_data_frame['Document no'].unique():
            my_clause_list = []
            my_clause_list_df = cause_data_frame[cause_data_frame['Document no'] == index]['Clauses']
            for ind in my_clause_list_df.index:
                my_clause_list.append([my_clause_list_df[ind]])
            while len(my_clause_list) < conf.sentence_padding:
                my_clause_list.append(["<EOF>"])
            my_clause_list = my_clause_list[:conf.sentence_padding]
            cause = int(cause_data_frame[cause_data_frame['Document no'] == index]['emotion'].values.tolist()[0])
            final_list.append([my_clause_list, cause])

        return np.asarray(final_list)

    def prepare_data_for_emotion_cause(self):
        cause_data_frame = self.__df[0][['Document no', 'emotion','cause', 'Clauses']]
        cause_data_frame['Clauses'].replace('\n', '', regex=True, inplace=True)
        cause_data_frame['Clauses'] = cause_data_frame['Clauses'].str.lower()
        final_list = []

        for index in cause_data_frame['Document no'].unique():
            my_clause_list = []
            my_clause_list_df = cause_data_frame[cause_data_frame['Document no'] == index]['Clauses']
            for ind in my_clause_list_df.index:
                my_clause_list.append([my_clause_list_df[ind]])
            while len(my_clause_list) < conf.sentence_padding:
                my_clause_list.append(["<EOF>"])
            my_clause_list = my_clause_list[:conf.sentence_padding]
            emotion = int(cause_data_frame[cause_data_frame['Document no'] == index]['emotion'].values.tolist()[0])
            cause = int(cause_data_frame[cause_data_frame['Document no'] == index]['cause'].values.tolist()[0])
            final_list.append([my_clause_list,(emotion, cause)])

        return np.asarray(final_list)
