"""
A Generic BiLSTM Model for detecting cause/emotion in sentences.
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from Sentence_Embedding import embedding_generation as eg
from Utility.configuration import device


class BiLSTM(nn.Module):

    __inner_layer_dim = 64

    def __init__(self, embedder_name,hidden_dim, target_size):
        super(BiLSTM,self).__init__()
        self.embedding = eg.Embedding(embedder_name)
        self.lstm = nn.LSTM(self.embedding.get_embedding_length(), hidden_dim, bidirectional=True)
        self.final_layer = nn.Linear(2*hidden_dim, target_size)

    def forward(self,docs):
        sentence_emebedding = []
        for sentence in docs:
            embeds = self.embedding.get_embedding(sentence[0])
            sentence_emebedding.append(embeds.view(-1))
        final_embedding = torch.stack(sentence_emebedding,dim=0).to(device=device)
        lstm_out, _ = self.lstm(final_embedding.view(len(docs),1,-1))
        tag_scores = F.softmax(self.final_layer(lstm_out.view(len(docs), -1)),dim=0)
        return tag_scores