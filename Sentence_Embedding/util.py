#https://huggingface.co/sentence-transformers - for available models of BERT
BERT_PRETRAINED_MODEL = 'distilbert-base-nli-mean-tokens'
USE_MODEL = 'distiluse-base-multilingual-cased-v2'