# !pip install -U sentence-transformers
# for BERT and USE
from sentence_transformers import SentenceTransformer
import torch
from Sentence_Embedding.util import *
# for GPT
from transformers import GPT2Tokenizer, GPT2Model
from sentence_transformers import SentenceTransformer


class Embedding:
    __model = None
    __tokenizer_type = None

    def __init__(self, tokenizer_type):
        switcher = {
            "BERT": self.__bert_model_init,
            "USE": self.__use_model_init,
            "GPT2": self.__gpt2_model_init
        }
        if tokenizer_type not in switcher:
            raise Exception("Tokenizer type not available.")
        self.__tokenizer_type = tokenizer_type
        switcher.get(tokenizer_type)()

    def __bert_model_init(self):
        self.__model = SentenceTransformer(BERT_PRETRAINED_MODEL)

    def __use_model_init(self):
        self.__model = SentenceTransformer(USE_MODEL)

    def __gpt2_model_init(self):
        self.__tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
        self.__model = GPT2Model.from_pretrained('gpt2', return_dict=True)

    # create GPT sentence embedding by mean pooling
    def __mean_pooling(self, model_output, attention_mask):
        token_embeddings = model_output[0]  # First element of model_output contains all token embeddings
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
        return sum_embeddings / sum_mask

    def __bert_model_embedding(self, sentence):
        embedding = self.__model.encode(sentence)
        embedding_tensor = torch.tensor(embedding).unsqueeze(0)
        return embedding_tensor

    def __use_model_embedding(self, sentence):
        embedding = self.__model.encode(sentence)
        embedding_tensor = torch.tensor(embedding).unsqueeze(0)
        return embedding_tensor

    def __gpt2_model_embedding(self, sentence):
        inputs = self.__tokenizer(sentence, return_tensors="pt")
        # Compute token embeddings
        with torch.no_grad():
            outputs = self.__model(**inputs)

        sentence_embedding = self.__mean_pooling(outputs, inputs['attention_mask'])
        return sentence_embedding

    '''
        create sentence embedding depending on the tokenizer_type
        BERT - for Bert
        USE - for Universal Sentence Encoder
        GPT2 - for gpt2 sentence embedding 
    '''

    def get_embedding(self, sentence):
        switcher = {
            "BERT": self.__bert_model_embedding,
            "USE": self.__use_model_embedding,
            "GPT2": self.__gpt2_model_embedding
        }
        return switcher.get(self.__tokenizer_type)(sentence)

    def get_embedding_length(self):
        switcher = {
            "BERT": 768,
            "USE": 512,
            "GPT2": 768
        }
        return switcher.get(self.__tokenizer_type)


'''
@inproceedings{reimers-2020-multilingual-sentence-bert,
    title = "Making Monolingual Sentence Embeddings Multilingual using Knowledge Distillation",
    author = "Reimers, Nils and Gurevych, Iryna",
    booktitle = "Proceedings of the 2020 Conference on Empirical Methods in Natural Language Processing",
    month = "11",
    year = "2020",
    publisher = "Association for Computational Linguistics",
    url = "https://arxiv.org/abs/2004.09813",
}

@inproceedings{reimers-2019-sentence-bert,
    title = "Sentence-BERT: Sentence Embeddings using Siamese BERT-Networks",
    author = "Reimers, Nils and Gurevych, Iryna",
    booktitle = "Proceedings of the 2019 Conference on Empirical Methods in Natural Language Processing",
    month = "11",
    year = "2019",
    publisher = "Association for Computational Linguistics",
    url = "http://arxiv.org/abs/1908.10084",
}
'''
