1 9
 (7,9)
1, null, null, when I see the suggestion is accepted
2, null, null, when the head of the ministry wrote to me
3, null, null, I know I am doing my part for the development of this country
4,null,null,27th
5, null, null, ordinary worker Bai Jinyue of Xingtai Iron and Steel Co., Ltd., Hebei Province
6, null, null, holding the thank you letters that various national ministries and commissions have given him over the years
7, happiness, excitement, excitement to the China News Network reporter
8,null,null, 27 years
9, null, null, my suggestions accepted and adopted by the Ministry of Public Security, the State Administration for Industry and Commerce, the National Science and Technology Commission, the Ministry of Science and Technology, the Ministry of Health, the National Development and Reform Commission and other ministries and commissions
2 9
 (9,8)
1, null, null, according to the introduction of Bai Jinyue
2,null,null, since 1988
3,Null, null, he put forward more than 1,000 rationalization suggestions to various ministries and commissions
4, null, null, and have been adopted by various ministries many times
5, null, null, won the first prize of the National Twelfth Five-Year Plan for Suggestions and Suggestions, the third prize of Hebei Province for Suggestions and Suggestions
6, null, null, same day
7, null, null, talk to a reporter from Chinanews about the original intention of making suggestions
8,null,null, platinum jumps into memories
9, happiness, excitement, and slightly excited
3 15
 (12,12)
1,null,null, the morning of June 3, 2002
2, null, null, the duty friend Zeng Youwei receives the report
3,null,null, in a woods in Zoumaying Village, Xiaotang, Shishan Town, there is a little boy over 2 years old lying on the grass
4, null, null, unclaimed
5, null, null, Zeng Youwei rushed to the scene immediately
6, null, null, I can only see the grass in the forest
7, null, null, the little boy was wrapped in a towel
8, null, null, very weak
9, null, null, don't cry or make trouble
10, null, null, small eyeballs quietly looking at the police uncle in front of them
11,Null, null, Zeng Youwei opens the wrapped scarf
12, sadness, distressed, the little boy's chest is so thin that it hurts
13,null, null, there is a note with the date of birth and a letter next to me
14, null, null, Zeng Youwei realized
15, null, null, this may be a child who was abandoned due to illness
4 12
 (12,9), (12,10), (12,11)
1, null, null, to save the woman as soon as possible
2, null, null, the commander immediately worked out a rescue plan
3,null,null, the first group laid a life-saving mat downstairs
4, null, null, and evacuate unrelated people around
5,null,null, another group of team members quickly climbed to the 6th floor
6, null, null, persuade women in the building
7, null, null, in the process of persuading
8, null, null, fire officers and soldiers understand
9, null, null, the woman is due to the other party defaulting on the project payment
10, null, null, and urgently need money at home
11, null, null, life pressure
12, sadness, helpless, helplessly chose to jump off the building to commit suicide
5 7
 (7,6)
1, null, null, relying on the support of parents and the money saved by working at school
2, null, null, the two invested a total of more than 500,000 yuan
3, null, null, set up a chicken farm
4,null,null, started selling products in November last year
5, null, null, 3 months sales have reached more than 500,000 yuan
6, null, null, have such a good beginning
7, happiness, pride, the two are both proud and full of confidence in the prospects
6 12
 (6,6), (9,12)
1,null,null, June 2013
2, null, null, Wu Shuliang, who has worked hard in Shenzhen for 10 years, finally got the Shenzhen Hukou
3, null, null, son Wu Tong also moved to Shenzhen
4, null, null, but his wife Ding Weiqing must wait for Wu Shuliang to enter the household for two years before moving with him
5, null, null, half a year later
6, happiness, joy, the joy of getting the index
7, null, null, because of the bad news of Lao Wu suffering from advanced lung cancer
8, null, null, replaced
9, sadness, worry, is his worry about his life
10,null,null, the doctor's sentence is 36 months
11, null, null, which means
12, null, null, old Wu may not be able to wait for his wife to move into Shenzhen
8 8
 (1,2), (1,3), (8,5), (8,7)
1, Disgust, life is better than death, and life is better than death. Wu often thinks after suffering from cancer.
2, null, null, due to large area bone metastasis
3,Null, null, Lao Wu struggles with severe pain every day
4,null,null, from October 2012 to present
5, null, null, Lao Wu has been fighting advanced lung cancer for 30 months
6, null, null, this length of time
7, null, null, nearly 10 times more than the doctor's original death sentence
8,surprise, surprised, Wang Jin, a doctor in Shenzhen Ning Nursing Home, was also surprised
10 12
 (5,4)
1,null,null, in order to persist until the wife enters the house
2, null, null, old Wu has exhausted all his strength
3,Null, null, Lao Wu is well known for his anti-cancer deeds
4, null, null, if Lao Wu does not update Weibo for a long time
5, Fear, worry, some netizens will worry about whether Lao Wu has gone
6,null,null, end of 2013
7, null, null, Lao Wu hasn't updated Weibo for more than two months
8, null, null, some netizens worriedly asked him how he is doing
9, null, null, see netizens asking questions
10, null, null, he is experiencing a period of severe pain of targeted drug resistance
11, null, null, still struggling to type four words to live
12, null, null, alive
11 9
 (5,2), (5,4)
1, null, null, the whole family currently lives on her salary of more than two thousand yuan
2, null, null, have a lot of debt to see a doctor
3, null, null, if I am no longer
4, null, null, it will be more difficult for a wife without a Shenzhen household registration
5, sadness, helplessness, in short, helplessness and sadness
6, null, null, but I am also very grateful to those netizens who helped me volunteers in the nursing home
7, null, null, and the family's perseverance
8, null, null, now I can only grit my teeth and continue
9, null, null, hope to achieve my wish
12 10
 (7,7), (7,8)
1, null, null, the whole family currently lives on her salary of more than two thousand yuan
2, null, null, have a lot of debt to see a doctor
3, null, null, if I am no longer
4, null, null, wife does not have Shenzhen household registration
5, null, null, day will be harder
6, null, null, in short, helpless and sad
7, happiness, thanks, but I am also very grateful to those netizens who help me volunteers in the Ningxia Home
8, null, null, and the family's perseverance
9, null, null, now I can only grit my teeth and continue
10, null, null, hope to achieve my wish
13 7
 (6,5)
1,null,null, afternoon of April 14
2, null, null, some Weibo netizens broke the news
3, Null, null, several dead cats hanging in a pharmaceutical factory in Taihu, Tongzhou
4, null, null, and allotment of pictures
5, Null, null, many netizens accused of such abuse of cats
6, disgust, inhumane, too inhumane
7, null, null, and netizens suspect that this is the factory's method of handling stray cats
14 7
 (2,1), (6,6)
1, null, null, husband standing under the building and quarreling with his wife on the 6th floor
2, happiness, excitement, wife slipped off the balcony when she was excited
3,null,null, but luckily hung on the drying rack on the 4th floor
4,null,null, husband shouts immediately, hurry up
5,null,null, I love you
6, happiness, moved, wife was moved by warm words
7, null, null, self-help success
16 10
 (5,3)
1, null, null, at 11 o'clock at night on the 19th of the same month
2, null, null, Han Nan calls Xiaowen to meet in front of a convenience store
3, null, null, Xiaowen mistakenly believed that the other party was her boyfriend Qiu and went to the appointment
4,null,null, after 2 people meet
5, Surprise, surprised, Xiaowen was surprised
6, null, null, but also accept Han Nan's proposal
7, null, null, chat and walk in the park
8,null,null, it didn't take long for Han Nan to force Xiaowen to the toilet for sexual assault
9, null, null, but failed due to Xiaowen's resistance
10,null,null, Han Nan forcefully pulls the girl to the stairwell behind the command platform of the sports field and succeeds in sexual assault
17 11
 (7,6)
1, null, null, five years
2, null, null, the child has never passed a drop of urine
3, null, null, itching and pain on the body every day, the chest is stuffy and can’t breathe
4, null, null, often can't sleep in the middle of the night
5,null,null, I will take her out for a walk on a motorcycle
6, null, null, looking at the daughter who was once excellent in all aspects, now it is like this
7, sadness, distressed, Zhang Zhiying distressed
8,null,null, during school
9, null, null, Zhang Qiongyuan is the top three in every exam
10, null, null, over the years
11, null, null, has been a class cadre
18 2
 (2,2)
1, null, null, until now
2, disgust, self-blame, Zhang Zhiying also blames herself for not being able to buy a big bowl of noodles for her daughter
19 12
 (6,7)
1, null, null, some time ago
2, null, null, after the doctor who was treated at Peking University People’s Hospital called and asked about Qiong Yuan’s condition
3, null, null, say there is hope for children
4, null, null, it is recommended to do a kidney transplant
5, null, null, hope is here
6, sadness, can't be happy, but Zhang Zhiying can't be happy
7, null, null, because Zhang Qiongyuan is not his biological daughter
8, null, null, in one check
9, null, null, Zhang Qiongyuan learned that he was blood type O
10, null, null, and the father is type A
11, null, null, when the mother is AB type
12,Null, null, smart she instantly understood the meaning