# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 13:27:57 2020

@author: Aruvi

"""

from DataPreprocessing.file_import import *
import pandas as pd

# This file converts the raw data into standard format.
# This file returns two set of data frames.
# 1. Clause Data -> Here the data is divided on clause level.
# 2. Document Data  -> Here each sample is a document.

def data_proc():
    with open(conf.file_import_path, encoding="utf8") as f:
        inp = f.readlines()

    main_df = DataFrame(inp, columns=['all_data'])

    main_df['Doc_clause'] = '-'
    j = 0
    for i in range(len(main_df['all_data'])):
        if "," not in main_df['all_data'][i]:
            main_df['Doc_clause'][j] = main_df['all_data'][i]
        else:
            main_df['Doc_clause'][j] = main_df['Doc_clause'][i - 1]
        j = j + 1

    main_df['em_cs'] = '-'
    j = 0
    for i in range(len(main_df['all_data'])):
        try:
            if "(" in main_df['all_data'][i][1]:
                main_df['em_cs'][j] = main_df['all_data'][i]
            else:
                main_df['em_cs'][j] = main_df['em_cs'][i - 1]

        except:
            main_df['em_cs'][j] = '-'
        j = j + 1

    main_df = main_df[~main_df.all_data.str.startswith(' (')]
    main_df = main_df[main_df['all_data'].str.contains(',')]

    main_df['Document no'] = main_df['Doc_clause'].str.split(' ').str[0]
    main_df['Total clause'] = main_df['Doc_clause'].str.split(' ').str[1]

    main_df['emotion'] = main_df['em_cs'].str.split(',').str[0]
    main_df['cause'] = main_df['em_cs'].str.split(',').str[1]
    main_df['emotion'] = main_df['emotion'].str.split('(').str[1]
    main_df['cause'] = main_df['cause'].str.split(')').str[0]

    main_df['Clause no'] = main_df['all_data'].str.split(',').str[0]

    main_df['Clauses'] = main_df['all_data'].str.split(',', 1).str[1]
    main_df['Clauses'] = main_df['Clauses'].str.split(',', 1).str[1]
    main_df['Clauses'] = main_df['Clauses'].str.split(',', 1).str[1]

    clause_data = main_df[['Document no', 'emotion', 'cause', 'Clause no', 'Clauses']]

    doc_data = main_df[['Document no', 'Clauses']]
    doc_data['Document no'] = doc_data['Document no'].astype(str)
    doc_data['Clauses'] = doc_data['Clauses'].fillna(" ")

    grouped_df = doc_data.groupby("Document no")
    grouped_lists = grouped_df["Clauses"].agg(lambda column: ". ".join(column))
    grouped_lists = pd.DataFrame(grouped_lists)
    grouped_lists.reset_index(inplace=True)
    grouped_lists = grouped_lists.rename(columns={'index': 'Document no'})
    doc_data = grouped_lists
    doc_data['Document no'] = doc_data['Document no'].astype(int)
    doc_data = doc_data.sort_values(by=['Document no'])
    clause_data['Clause no'] = clause_data['Clause no'].astype(int)

    drop_clause = clause_data[(clause_data['Clause no'] > 25)]

    drop_clause = drop_clause[["Document no"]]

    drop_clause.drop_duplicates()

    drop_docs = drop_clause['Document no'].to_list()

    clause_data = clause_data[~clause_data['Document no'].isin(drop_docs)]

    doc_data = doc_data[~doc_data['Document no'].isin(drop_docs)]

    return clause_data, doc_data
