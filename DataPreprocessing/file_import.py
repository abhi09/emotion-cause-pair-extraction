# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 13:24:17 2020

@author: Aruvi
"""


from pandas import DataFrame
from Utility import configuration as conf

# This file imports data from a file
def import_file():
    with open(conf.file_import_path, encoding="utf8") as f:
        inp = f.readlines()
        
    main_df = DataFrame(inp,columns=['all_data'])
    return main_df