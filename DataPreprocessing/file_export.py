# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 13:34:28 2020

@author: Aruvi
"""

from DataPreprocessing.data_process import *

# This file exports data to a file.
clause_data, doc_data = data_proc()

clause_data.to_csv("processed_clause_data.csv")
doc_data.to_csv("processed_doc_data.csv")